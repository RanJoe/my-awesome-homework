package Users

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

// FormToUser -- fills a User struct with submitted form data
// params:
// r - request reader to fetch form data or url params (unused here)
// returns:
// User struct if successful
// array of strings of errors if any occur during processing
func FormToUser(r *http.Request) (User, []string) {
	var user User
	var errStr, ageStr string
	var errs []string
	var err error
	body, _ := ioutil.ReadAll(r.Body)
	bodyString := string(body)

	fmt.Println("Data:")
	fmt.Println(bodyString)

	byt := []byte(bodyString)
	personMap := make(map[string]interface{})
	if err := json.Unmarshal(byt, &personMap); err != nil {
		panic(err)
	}
	//fmt.Println("Data after unmarshal:")
	//fmt.Println(personMap["firstname"])

	user.FirstName, errStr = processFormField(personMap, "firstname")
	errs = appendError(errs, errStr)
	user.LastName, errStr = processFormField(personMap, "lastname")
	errs = appendError(errs, errStr)
	user.Mobileno, errStr = processFormField(personMap, "mobileno")
	errs = appendError(errs, errStr)
	user.Gender, errStr = processFormField(personMap, "gender")
	errs = appendError(errs, errStr)

	ageStr, errStr = processFormField(personMap, "age")
	if len(errStr) != 0 {
		errs = append(errs, errStr)
	} else {
		user.Age, err = strconv.Atoi(ageStr)
		if err != nil {
			errs = append(errs, "Parameter 'age' not an integer")
		}
	}
	return user, errs
}

// LoginFormToUser -- fills a User struct with submitted login data
// params:
// r - request reader to fetch form data or url params (unused here)
// returns:
// User struct if successful
// array of strings of errors if any occur during processing
func LoginFormToUser(r *http.Request) (User, []string) {
	var user User
	var errStr string
	var errs []string
	//var err error
	body, _ := ioutil.ReadAll(r.Body)
	bodyString := string(body)
	fmt.Println("Data:")
	fmt.Println(bodyString)

	byt := []byte(bodyString)
	personMap := make(map[string]interface{})
	if err := json.Unmarshal(byt, &personMap); err != nil {
		panic(err)
	}
	fmt.Println("Data after unmarshal:")
	fmt.Println(personMap["firstname"])

	user.FirstName, errStr = processFormField(personMap, "firstname")
	errs = appendError(errs, errStr)
	user.LastName, errStr = processFormField(personMap, "lastname")
	errs = appendError(errs, errStr)
	return user, errs
}

func appendError(errs []string, errStr string) []string {
	if len(errStr) > 0 {
		errs = append(errs, errStr)
	}
	return errs
}

func processFormField(pmap map[string]interface{}, field string) (string, string) {
	//fmt.Println("hola")
	fieldData := pmap[field].(string)
	if len(fieldData) == 0 {
		return "", "Missing '" + field + "' parameter, cannot continue"
	}
	return fieldData, ""
}
