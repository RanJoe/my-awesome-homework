package Users

import (
	"github.com/gocql/gocql"
)

// User struct to hold profile data for our user
type User struct {
	ID        gocql.UUID `json:"id"`
	FirstName string     `json:"firstname"`
	LastName  string     `json:"lastname"`
	Age       int        `json:"age"`
	Mobileno  string     `json:"mobileno"`
	Gender    string     `json:"gender"`
}

type PostUser struct {
	ID        gocql.UUID `json:"id"`
	FirstName string     `json:"firstname"`
	LastName  string     `json:"lastname"`
	Age       string     `json:"age"`
	Mobileno  string     `json:"mobileno"`
	Gender    string     `json:"gender"`
}

// GetUserResponse to form payload returning a single User struct
type GetUserResponse struct {
	User User `json:"user"`
}

// AllUsersResponse to form payload of an array of User structs
type AllUsersResponse struct {
	Users []User `json:"users"`
}

// NewUserResponse builds a payload of new user resource ID
type NewUserResponse struct {
	ID gocql.UUID `json:"id"`
}

// ErrorResponse returns an array of error strings if appropriate
type ErrorResponse struct {
	Errors []string `json:"errors"`
}
