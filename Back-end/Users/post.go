package Users

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/RanJoel/awesomehomework/back-end/Cassandra"
	"github.com/gocql/gocql"
)

// Post -- handles POST request to /users/new to create new user
// params:
// w - response writer for building JSON payload response
// r - request reader to fetch form data or url params
func Post(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	if !(r.Method == "OPTIONS") {
		var errs []string
		var gocqlUuid gocql.UUID

		// FormToUser() is included in Users/processing.go
		// we will describe this later
		postUser, errs := FormToUser(r)

		// have we created a user correctly
		var created bool = false

		// if we had no errors from FormToUser, we will
		// attempt to save our data to Cassandra
		if len(errs) == 0 {
			fmt.Println("creating a new user")

			// generate a unique UUID for this user
			gocqlUuid = gocql.TimeUUID()

			// write data to Cassandra
			if err := Cassandra.Session.Query(`
		INSERT INTO users (id, firstname, lastname, age, mobileno, gender) VALUES (?, ?, ?, ?, ?, ?)`,
				gocqlUuid, postUser.FirstName, postUser.LastName, postUser.Age, postUser.Mobileno, postUser.Gender).Exec(); err != nil {
				errs = append(errs, err.Error())
			} else {
				created = true
			}
		}

		// depending on whether we created the user, return the
		// resource ID in a JSON payload, or return our errors
		if created {
			fmt.Println("user_id", gocqlUuid)
			json.NewEncoder(w).Encode(NewUserResponse{ID: gocqlUuid})
		} else {
			fmt.Println("errors", errs)
			json.NewEncoder(w).Encode(ErrorResponse{Errors: errs})
		}
	}

}

// PostLoginCheck -- handles POST request to /users/logincheck to check user's firstname and lastname
// params:
// w - response writer for building JSON payload response
// r - request reader to fetch form data or url params
func PostLoginCheck(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	if !(r.Method == "OPTIONS") {
		var errs []string
		fmt.Println("test")
		postUser, _ := LoginFormToUser(r)

		var userList []User
		m := map[string]interface{}{}

		query := "SELECT id,firstname,lastname,gender,age,mobileno FROM users where firstname='" + postUser.FirstName + "' ALLOW FILTERING;"
		//fmt.Println(query)
		iterable := Cassandra.Session.Query(query).Iter()
		for iterable.MapScan(m) {
			userList = append(userList, User{
				ID:        m["id"].(gocql.UUID),
				Age:       m["age"].(int),
				FirstName: m["firstname"].(string),
				LastName:  m["lastname"].(string),
				Mobileno:  m["mobileno"].(string),
				Gender:    m["gender"].(string),
			})
			m = map[string]interface{}{}
		}

		if postUser.FirstName == userList[0].FirstName && postUser.LastName == userList[0].LastName {
			fmt.Println("Yes")
			json.NewEncoder(w).Encode(userList)
		} else {
			fmt.Println("No")
			errs = append(errs, "User not found")
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("User not found"))
			//json.NewEncoder(w).Encode(ErrorResponse{Errors: errs})
		}

	}

}
