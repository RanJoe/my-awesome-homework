package main

import (
	"encoding/json"
	"net/http"

	"github.com/RanJoel/awesomehomework/back-end/Cassandra"
	"github.com/RanJoel/awesomehomework/back-end/Users"
	"github.com/gorilla/mux"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
)

func main() {

	//Utilize our Cassandra session from our subpackage
	CassandraSession := Cassandra.Session
	defer CassandraSession.Close()

	//corsObj := handlers.AllowedOrigins([]string{"*"})

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", heartbeat)
	router.HandleFunc("/users/new", Users.Post).Methods("POST", "OPTIONS")
	router.HandleFunc("/users/logincheck", Users.PostLoginCheck).Methods("POST", "OPTIONS")

	router.HandleFunc("/users", Users.Get).Methods("GET", "OPTIONS")
	router.HandleFunc("/users/{user_uuid}", Users.GetOne).Methods("GET", "OPTIONS")

	fasthttp.ListenAndServe("localhost:8080", fasthttpadaptor.NewFastHTTPHandler(router))
	//log.Fatal(http.ListenAndServe(":8080", handlers.CORS(corsObj)(router)))
}

/*tells JSON-encoder to rename the “Status” and “Code”
fields during the encoding process to their lowercase versions.*/
type heartbeatResponse struct {
	Status string `json:"status"`
	Code   int    `json:"code"`
}

/*tells gorilla/mux to use a JSON encoder on our output handle
and encode our data structure*/
func heartbeat(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(heartbeatResponse{Status: "OK", Code: 200})
}
