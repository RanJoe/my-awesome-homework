import React, { Component } from 'react';
//import { Route, Switch, Redirect, BrowserRouter } from "react-router-dom";
import { Route, BrowserRouter } from 'react-router-dom';
import SignUpForm from './pages/SignUpForm';
import SignInForm from './pages/SignInForm';

import './App.css';
import MemberPage from './pages/MemberPage';
import { createBrowserHistory } from "history";


const hist = createBrowserHistory();


class App extends Component {

  /*state = {
    users:[]
  }

  componentDidMount(){
    const url = 'http://localhost:8080/users';
    axios.get(url).then(response => response.data)
    .then((data) => {
      this.setState({ users:data})
      console.log(this.state.users)
    })
  }*/

  render() {
    return (
      <BrowserRouter history={hist}>
        
              <Route exact path="/" component={SignUpForm}>
              </Route>
              <Route path="/sign-in" component={SignInForm}>
              </Route>
              <Route path="/member" component={MemberPage}>
              </Route>
         
              
          
        
        </BrowserRouter>
    );
  }
}

export default App;
