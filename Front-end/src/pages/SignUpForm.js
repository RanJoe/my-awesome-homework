import React, { Component } from 'react';
import { Link, NavLink} from 'react-router-dom';
import axios from 'axios';

class SignUpForm extends Component {
    constructor() {
        super();

        this.state = {
            firstname: '',
            lastname: '',
            gender: '',
            age: '',
            mobileno: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });
    }

    handleSubmit(e) {

      console.log(this.state);
      axios.post('http://localhost:8080/users/new',this.state,{
        headers: {
            "Content-Type": "application/json",
        }
    })
      .then(function (response) {
        alert('Registration completed successfully!\nPlease log in to manage your account');
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
      /*axios({
        method: 'POST',
        url: 'http://localhost:8080/users/new',
        data: this.state,
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        }
        })
        .then(function (response) {
          alert('Hello');
          console.log('The form was submitted with the following data:');
          console.log(this.state);
        })
        .catch(function (error) {
            alert('Error ');
            console.log(error);
        });*/
        e.preventDefault();
        
    }

    

    render() {
        return (
        <div className="App">
        <div className="App__Aside"></div>
          <div className="App__Form">
        <div className="FormCenter">
          <div className="PageSwitcher">
                <NavLink to="/sign-in" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Sign In</NavLink>
                <NavLink exact to="/" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Sign Up</NavLink>
              </div>

              <div className="FormTitle">
                  <NavLink to="/sign-in" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Sign In</NavLink> or <NavLink exact to="/" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Sign Up</NavLink>
              </div>
            <form onSubmit={this.handleSubmit} className="FormFields">
              <div className="FormField">
                <label className="FormField__Label" htmlFor="firstname">First Name</label>
                <input type="text" id="firstname" className="FormField__Input" placeholder="Enter your full name" name="firstname" value={this.state.firstname} onChange={this.handleChange} />
              </div>
              <div className="FormField">
                <label className="FormField__Label" htmlFor="lastname">Last Name</label>
                <input type="text" id="lastname" className="FormField__Input" placeholder="Enter your last name" name="lastname" value={this.state.lastname} onChange={this.handleChange} />
              </div>
              <div className="FormField">
                <label className="FormField__Label" htmlFor="gender">Gender</label>
                <select className="FormField__Input" id="gender" name="gender" value={this.state.gender} onChange={this.handleChange}>
                  <option className="FormField__Input1"> -- Select your gender -- </option>
                  <option value="Male" className="FormField__Input1">Male</option>
                  <option value="Female" className="FormField__Input1">Female</option>
                </select>
              </div>
              <div className="FormField">
                <label className="FormField__Label" htmlFor="age">Age</label>
                <input type="text" id="age" className="FormField__Input" placeholder="Enter your age" name="age" value={this.state.age} onChange={this.handleChange} />
              </div>
              <div className="FormField">
                <label className="FormField__Label" htmlFor="mobileno">Phone Number</label>
                <input type="text" id="mobileno" className="FormField__Input" placeholder="Enter your phone number" name="mobileno" value={this.state.phoneno} onChange={this.handleChange} />
              </div>

              

              <div className="FormField">
                  <button className="FormField__Button mr-20">Sign Up</button> <Link to="/sign-in" className="FormField__Link">I'm already member</Link>
              </div>
            </form>
          </div>
          </div>
          </div>
        );
    }
}
export default SignUpForm;