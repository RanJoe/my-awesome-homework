import React, { Component } from 'react';
import axios from 'axios';
import Table from './Table';

class MemberPage extends Component {

  
  
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }
    
      componentDidMount(){
        const url = 'http://localhost:8080/users/6c758a3d-d70c-11e9-8d98-8cec4b201e6e';
        axios.get(url)
        .then(response => 
          response.data
        )
        .then((data) => {
          this.setState({ users:data})
          console.log(this.state.users)
          
        })
        .catch(function (error) {
          console.log(error);
        })
      }
     //{this.state.users.map((user) => (<label className="FormField__Label" >{user.lastname}</label>))}
    render() {
        return (
          <div className="App">
            <div className="App__FormLeft">
              <div className="FormTitle">
        <div className="skewed-bg"><label className="FormTitle__Left" >Welcome!</label></div>
        
          </div>
        <div className="FormCenter">
        

          <div className="FormFieldLeft">
              <label className="FormField__LabelLeft" >firstname:</label>
              {this.state.users.map((user) => (<label className="FormTitle__Firstname" >{user.firstname}</label>))}
            </div>

            <div className="FormFieldLeft">
              <label className="FormField__LabelLeft" >lastname:</label>
              {this.state.users.map((user) => (<label className="FormTitle__Firstname" >{user.lastname}</label>))}
            </div>

            <div className="FormFieldLeft">
              <label className="FormField__LabelLeft" >gender:</label>
              {this.state.users.map((user) => (<label className="FormTitle__Firstname" >{user.gender}</label>))}
            </div>

            <div className="FormFieldLeft">
              <label className="FormField__LabelLeft" >age:</label>
              {this.state.users.map((user) => (<label className="FormTitle__Firstname" >{user.age}</label>))}
            </div>
            <div className="FormFieldLeft">
              <label className="FormField__LabelLeft" >mobile number:</label>
              {this.state.users.map((user) => (<label className="FormTitle__Firstname" >{user.mobileno}</label>))}
            </div>

          </div>
        </div>

            <div className="App__FormRight">
            <div className="FormCenter">
            <Table/>
              
            </div>
            </div>
          </div>
        );
    }
}
export default MemberPage;

