import React from 'react';
import MaterialTable from 'material-table';
import axios from 'axios';


  

export default () => {

  
  //console.log(users);

    const [state, setState] = React.useState({
      columns: [
        { title: 'Firstname', field: 'firstname' },
        { title: 'Lastname', field: 'lastname' },
        { title: 'Gender', field: 'gender' },
        { title: 'Age', field: 'age', type: 'numeric'},
        { title: 'Mobile Number', field: 'mobileno' },
      ],
      data: [{ firstname: 'Visarut', lastname: 'Wwww', gender: 'Male', age: 25 , mobileno: '0875641221'},
      { firstname: 'Tanapol', lastname: 'Pumlumchiak', gender: 'Male', age: 26 , mobileno: '0992504664'},
      { firstname: 'Ryan', lastname: 'Reynold', gender: 'Male', age: 56 , mobileno: '111111111'},
      { firstname: 'Catheryn', lastname: 'Kat', gender: 'Female', age: 31 , mobileno: '024465893'},
      { firstname: 'Kittima', lastname: 'Srisupan', gender: 'Female', age: 26 , mobileno: '0841569889'},
     ]
    });
    //console.log(users.user)
    //console.log(state.data);
    return (
      <MaterialTable
        title="Manage your database"
        columns={state.columns}
        data={state.data}
        editable={{
          onRowAdd: newData =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                const data = [...state.data];
                data.push(newData);
                setState({ ...state, data });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                const data = [...state.data];
                data[data.indexOf(oldData)] = newData;
                setState({ ...state, data });
              }, 600);
            }),
          onRowDelete: oldData =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                const data = [...state.data];
                data.splice(data.indexOf(oldData), 1);
                setState({ ...state, data });
              }, 600);
            }),
        }}
      />
    );
  }