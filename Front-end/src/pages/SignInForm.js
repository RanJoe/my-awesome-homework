import React, { Component } from 'react';
import { Link, NavLink, } from 'react-router-dom';
import axios from 'axios';

class SignInForm extends Component {
    constructor() {
        super();

        this.state = {
            firstname: '',
            lastname: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

   

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });
    }

    handleSubmit(e) {
      var self = this;
      e.preventDefault();  
      axios.post('http://localhost:8080/users/logincheck',this.state,{
        headers: {
            "Content-Type": "application/json",
        }
    })
      .then(function (response) {
        alert('Login Succesful!');
        console.log(response);
        self.props.history.push('/member')
      })
      .catch(error => {
        alert('Login Failed!\nPlease check your firstname or lastname');
        console.log(error.response)
        
    });
  
    
    }

    render() {
        return (
          <div className="App">
        <div className="App__Aside"></div>
          <div className="App__Form">
        <div className="FormCenter">

          <div className="PageSwitcher">
            <NavLink to="/sign-in" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Sign In</NavLink>
            <NavLink exact to="/" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Sign Up</NavLink>
          </div>

          <div className="FormTitle">
            <NavLink to="/sign-in" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Sign In</NavLink> or <NavLink exact to="/" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Sign Up</NavLink>
          </div>

          <form onSubmit={this.handleSubmit} className="FormFields">

            <div className="FormField">
              <label className="FormField__Label" htmlFor="firstname">First Name</label>
              <input type="text" id="firstname" className="FormField__Input" placeholder="Enter your first name" name="firstname" value={this.state.firstname} onChange={this.handleChange} />
            </div>

            <div className="FormField">
              <label className="FormField__Label" htmlFor="lastname">Last Name</label>
              <input type="text" id="lastname" className="FormField__Input" placeholder="Enter your last name" name="lastname" value={this.state.lastname} onChange={this.handleChange} />
            </div>

            <div className="FormField">
              <button className="FormField__Button mr-20">Sign In</button> <Link to="/" className="FormField__Link">Create an account</Link>
            </div>

          </form>
          
        </div>
        </div>
        </div>
        );
    }
}

export default SignInForm;