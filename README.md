**--Edit History--**
- **05/09/19:** Initially created the project
- **06/09/19:** Tried the back-end using Golang and Cassandra
- **07/09/19:** Tried the front-end using Reactjs
- **09/09/19:** Tried connecting front-end and back-end
- **10/09/19:** Completed registration page using "POST" method
- **11/09/19:** Completed login page using "POST" method
- **12/09/19:** Show user details in member page using "GET" method
- **13/09/19:** User can "modify" their details in member page using "PUT" method
- **14/09/19:** User can "delete" others users in member page using "DELETE" method

**--Project Description--**<br />
A homework of designed website using React framework as front-end ,<br />
Golang as back-end and Cassandra as a database for storing user data.